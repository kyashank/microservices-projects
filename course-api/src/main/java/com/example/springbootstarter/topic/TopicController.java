package com.example.springbootstarter.topic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class TopicController {
	
	@Autowired
	private TopicService topicservice;

	@RequestMapping("/topics")
	public List<Topic> getAllTopics()
	{ 
        return topicservice.getAllTopics() ;	
	}
	
	@RequestMapping("/topics/{enteredId}")
	public Topic getTopic(@PathVariable("enteredId") String id)
	{
		return topicservice.getTopic(id);
	}
	
	@RequestMapping( method=RequestMethod.POST , value="/topics" )
	public void addTopic(@RequestBody Topic topic)
	{
		topicservice.addTopic(topic);
	}
	
	@RequestMapping( method=RequestMethod.PUT , value="/topics/{enteredId}" )
	public void updateTopic(@RequestBody Topic topic , @PathVariable("enteredId") String id)
	{
		topicservice.updateTopic(id,topic);
	}
	
	@RequestMapping( method=RequestMethod.DELETE , value="/topics/{enteredId}" )
	public void deleteTopic(@PathVariable("enteredId") String id)
	{
		topicservice.deleteTopic(id);
	}
	
	@RequestMapping("/topics/topic/{enteredName}")
	public Topic getTopicbyname(@PathVariable("enteredName") String topic)
	{
		System.out.println("inside name");
		return topicservice.getTopicbyname(topic);
	}
	
	
}

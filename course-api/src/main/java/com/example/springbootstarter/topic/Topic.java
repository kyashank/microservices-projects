package com.example.springbootstarter.topic;

public class Topic {
	
	private String id;
	private String topic;
	private String description;
	
	public Topic() {
		// TODO Auto-generated constructor stub
	}

	public Topic(String id, String topic, String description) {
		super();
		this.id = id;
		this.topic = topic;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Topic [id=" + id + ", topic=" + topic + ", description=" + description + "]";
	}
	
	

}

package com.example.springbootstarter.topic;

import static org.assertj.core.api.Assertions.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

			private List<Topic> topics = new ArrayList<>(Arrays.asList(
			
			new Topic("1","Spring Framework","Spring Framework Description"),  
	        new Topic("2","java","java Framework Description"),
	        new Topic("3","c Framework","c Framework Description")
			));
			
			public List<Topic> getAllTopics()
			{
				return topics;
			}

			public Topic getTopic(String id)
			{
				return topics.stream().filter(t->t.getId().equals(id)).findFirst().get();
			}
			
			public void addTopic(Topic topic)
			{
				topics.add(topic);
			}

			public void updateTopic(String id, Topic topic) {
				
				for(int i = 0; i <  topics.size(); i ++)
				{
					Topic t = topics.get(i);
					if(t.getId().equals(id))
					{
					topics.set(i, topic);
					return;
					}
				}
				
			}

			public void deleteTopic(String id) {
				
				topics.removeIf(t-> t.getId().equals(id));
				
			}
			
			public Topic getTopicbyname(String topic)
			{
			
				//return topics.stream().filter(t1->t1.getTopic().equals(topic)).findFirst().get();
			
				Topic t = null;
				for(int i = 0; i <  topics.size(); i ++)
				{
					t = topics.get(i);
					System.out.println("jajjaa-->" + t);
					System.out.println("Topic--->" + t.getTopic());
					if(t.getTopic().equals(topic))
						break;
					
					
				}
				return t;
			}
			
			
}

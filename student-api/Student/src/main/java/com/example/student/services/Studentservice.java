package com.example.student.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.student.model.Student;

@Service
public class Studentservice {

		
		private List<Student> students = new ArrayList<>(Arrays.asList(
			new Student("1", "yashank", "yes"),
			new Student("2","anubhhav","yes"),
			new Student("3","sahil","no"),
			new Student("4","ayush","no")
			));
	
	public List<Student> getAllStudents()
	{
		return students;
	}
	
	public Student getStudent(String roll)
	{
		Student s = null;
		for(int i = 0;i <= students.size();i++)
		{
			s = students.get(i);
			if(s.getRollno().equals(roll))
				break;
		}
		return s;
	}
	
	public void addStudent(Student student)
	{
		students.add(student);
	}
	
	public void updateStudent(String roll,Student student)
	{
		for(int i = 0; i <= students.size(); i++)
		{
			Student s = students.get(i);
			if(s.getRollno().equals(roll))
			{
				students.set(i, student);
				return;
			}
		}
	}
	
	public void deleteStudent(String roll)
	{
		students.removeIf(s->s.getRollno().equals(roll));
	}
	
}

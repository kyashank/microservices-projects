package com.example.student.model;

public class Student {
	
	private String rollno;
	private String name;
	private String examstatus;
	
	
	public Student() {
		// TODO Auto-generated constructor stub
	}

	public Student(String rollno, String name, String examstatus) {
		super();
		this.rollno = rollno;
		this.name = name;
		this.examstatus = examstatus;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExamstatus() {
		return examstatus;
	}

	public void setExamstatus(String examstatus) {
		this.examstatus = examstatus;
	}

	@Override
	public String toString() {
		return "Student [rollno=" + rollno + ", name=" + name + ", examstatus=" + examstatus + "]";
	}
	
	

}

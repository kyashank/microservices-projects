package com.example.student.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.student.model.Student;
import com.example.student.services.Studentservice;

@RestController
public class StudentController {
	
	@Autowired
	private Studentservice studentservice;
	
	@RequestMapping("/students")
	private List<Student> getAllStudents()
	{
		return studentservice.getAllStudents();
		
	}
	@RequestMapping("/students/{enteredroll}")
	private Student getStudent(@PathVariable("enteredroll") String roll)
	{
		return studentservice.getStudent(roll);
	}
	@RequestMapping(method=RequestMethod.POST,value="/students")
	private void addStudent(@RequestBody Student student)
	{
		 studentservice.addStudent(student);
	}
	@RequestMapping(method=RequestMethod.PUT,value="/students/{enteredroll}") 
	private void updateStudent(@RequestBody Student student, @PathVariable("enteredroll") String roll)
	{
		studentservice.updateStudent(roll, student);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/students/{enteredroll}")
	private void deleteStudent(@PathVariable("enteredroll") String roll)
	{
		studentservice.deleteStudent(roll);
	}
	
}

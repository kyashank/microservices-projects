package com.example.marks.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.marks.model.Marks;


@Service
public class Markservice {

	private List<Marks> markslist = new ArrayList<>(Arrays.asList(
			new Marks("1", "97%"),
			new Marks("2", "88"),
			new Marks("3", "92%"),
			new Marks("4", "23%")
			)); 
	
	public List<Marks> getAllMarks()
	{
		return markslist;
	}
	
	public Marks getMarks(String roll)
	{
		Marks m = null;
		for(int i = 0;i <= markslist.size();i++)
		{
			m = markslist.get(i);
			if(m.getRoll().equals(roll))
				break;
		}
		return m;
	}
	
	public void addMarks(Marks marks)
	{
		markslist.add(marks);
	}
	
	public void updateMarks(String roll,Marks marks)
	{
		for(int i = 0; i <= markslist.size(); i++)
		{
			Marks m = markslist.get(i);
			if(m.getRoll().equals(roll))
			{
				markslist.set(i, marks);
				return;
			}
		}
	}
	
	public void deletemarks(String roll)
	{
		markslist.removeIf(s->s.getRoll().equals(roll));
	}
	
}

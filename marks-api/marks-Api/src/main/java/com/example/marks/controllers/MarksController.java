package com.example.marks.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.marks.model.Marks;
import com.example.marks.services.Markservice;



@RestController
public class MarksController {
	
	@Autowired
	private Markservice markservice;
	
	@RequestMapping("/marks")
	private List<Marks> getAllMarks()
	{
		return markservice.getAllMarks();
		
	}
	@RequestMapping("/marks/{enteredroll}")
	private Marks getMarks(@PathVariable("enteredroll") String roll)
	{
		return markservice.getMarks(roll);
	}
	@RequestMapping(method=RequestMethod.POST,value="/marks")
	private void addMarks(@RequestBody Marks mark)
	{
		 markservice.addMarks(mark);
	}
	@RequestMapping(method=RequestMethod.PUT,value="/marks/{enteredroll}") 
	private void updateMarks(@RequestBody Marks mark, @PathVariable("enteredroll") String roll)
	{
		markservice.updateMarks(roll, mark);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/marks/{enteredroll}")
	private void deleteMarks(@PathVariable("enteredroll") String roll)
	{
		markservice.deletemarks(roll);
	}

}

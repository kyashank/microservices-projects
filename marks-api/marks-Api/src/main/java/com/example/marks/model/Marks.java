package com.example.marks.model;

public class Marks {
	
	private String roll;
	private String percentage;
	
	public Marks() {
		// TODO Auto-generated constructor stub
	}

	public Marks(String roll, String percentage) {
		super();
		this.roll = roll;
		this.percentage = percentage;
	}

	public String getRoll() {
		return roll;
	}

	public void setRoll(String roll) {
		this.roll = roll;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	@Override
	public String toString() {
		return "Marks [roll=" + roll + ", percentage=" + percentage + "]";
	}
	
	

}
